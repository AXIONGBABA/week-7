
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import pandas as pd

class Uncertainty_Reading():
    def __init__(self,experimental_path:str):

        self.experimental_path = experimental_path  + '\\experimental.csv' 

    def data_Reading(self):
        """
        with open(self.experimental_path,"r",encoding="utf-8") as data_files:
             data_base         = data_files.readlines()
             data_base.pop(0) #把第一个空列表删去
             vital_period_data = []

             for i in range(len(data_base)):
                 dataset = re.findall(r"\w+\b.\d+.\d+",data_base[i])[0] #匹配变量名+把列表转化成字符串以便spilt
                 vital_period_data.append(dataset.split(","))

             return vital_period_data
        """

        data_base = pd.read_csv(self.experimental_path,encoding='utf-8')
        #read_data_beg_buffer = data_base.iloc[:,:2].groupby("实验变量") #选取前两列,然后进行分组
        data_numpy = data_base.to_numpy() #把Dataframes转换为np imba到家了,这个命令
        data_numpy_length  = len(data_numpy)
        data_numpy_alldata = np.zeros(0)

        #读取转化后的np数组,把数据找出来输入新数组，然后输出数据数组 
        for i in range(data_numpy_length):
            data_numpy_data = np.zeros(0)
            data_numpy_uncertain_b = np.zeros(0)
            for j in data_numpy[i]:
                if type(j) == float or type(j) == int:
                    data_numpy_data = np.append(data_numpy_data, j)

            data_numpy_alldata = np.append(data_numpy_alldata, data_numpy_data)

        

        return data_numpy_alldata[~np.isnan(data_numpy_alldata)].reshape(data_numpy_length,-1) #去掉nan值并把数组转化为二维数组

    def data_Mean(self):
        data_mean_base = Uncertainty_Reading.data_Reading(self)
        data_mean      = np.zeros(len(data_mean_base))
        data_index     = np.zeros(len(data_mean_base))
        for i in range(len(data_mean_base)):
            data_mean[i]    = data_mean_base[i][:-1].mean()
            data_index[i]   = len(data_mean_base[i])-1

        return data_mean ,data_index

    
    def uncertainty_Type_a(self):
        uncertainty_type_a_index    = Uncertainty_Reading.data_Mean(self)[1] #data_Mean 里面的数据个数
        uncertainty_type_a_database = Uncertainty_Reading.data_Reading(self)
        uncertainty_type_a          = np.zeros(len(uncertainty_type_a_index))
        for i in range(len(uncertainty_type_a_database)):
            uncertainty_type_a[i] = uncertainty_type_a_database[i][:-1].std()/((uncertainty_type_a_index[i]) ** 1/2)

        return uncertainty_type_a


    def uncertainty_Type_b(self):
        uncertainty_type_b = Uncertainty_Reading.data_Reading(self)[:,-1]

        return uncertainty_type_b / (3)**1/2

    def uncertainty_Type_c(self):
        uncertainty_type_c = (Uncertainty_Reading.uncertainty_Type_a(self)**2
                             +Uncertainty_Reading.uncertainty_Type_b(self)**2)**1/2

        return uncertainty_type_c
    def uncertainty_Type_Relative(self):
        pass

    def data_Writting(self):
        pass


class visual_ization_subsystem(Uncertainty_Reading):
    pass




if __name__== "__main__":
    def uncertainty():
        experimental_path = os.path.dirname(os.path.abspath(__file__))
        database_location = Uncertainty_Reading(experimental_path)
        print(Uncertainty_Reading.data_Reading(database_location))
        print(Uncertainty_Reading.data_Mean(database_location))
        print(Uncertainty_Reading.uncertainty_Type_a(database_location))
        print(Uncertainty_Reading.uncertainty_Type_b(database_location))
        print(Uncertainty_Reading.uncertainty_Type_c(database_location))        
    uncertainty()
